//Initiallising node modules
var express = require("express");
var bodyParser = require("body-parser");
var sql = require("mssql");
var app = express();

// Body Parser Middleware
app.use(bodyParser.json());

//CORS Middleware
app.use(function (req, res, next) {
    //Enabling CORS 
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

//Setting up server
var server = app.listen(process.env.PORT || 6000, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
});

//Initialising connection string
var dbConfig = {
    user: "user",
    password: "psss",
    server: "serv@Dev",
    database: "database"
};

//Function to connect to database and execute query
var executeQuery = function (req, res) {
    sql.connect(dbConfig, function (err) {
        if (err) {
            console.log("Error while connecting database :- " + err);
            res.send(err);
            sql.close();
        }
        else {
            // create Request object
            var request = new sql.Request();
            // query to the database
            request.query(req, function (err, response) {
                if (err) {
                    console.log("Error while querying database :- " + err);
                    res.send(err);
                }
                else {
                    console.log(response);
                    res.send(response);

                    sql.close();
                }
            });
        }
    });
}

var get_login = function (req, res) {
    sql.connect(dbConfig, function (err) {
        if (err) {
            console.log("Error while connecting database :- " + err);
            res.send(err);
            sql.close();
        }
        else {
            // create Request object
            var request = new sql.Request();
            // query to the database
            request.query(req, function (err, response) {
                if (err) {
                    console.log("Error while querying database :- " + err);
                    res.send(err);
                }
                else {
                    var check_error = response.length;
                    if (check_error > 0) {
                        var data_tf = [{
                            status: 'OK',
                            api: 'LOGIN',
                            size: check_error,
                            data: response
                        }]
                        console.log("log : status 500");
                        res.send(data_tf);
                        sql.close();
                    }
                    else {
                        var data_tf = [{
                            status: 'DATA FAIL',
                            api: 'LOGIN',
                            size: 0,
                            data: response
                        }]
                        console.log("log : status 400");
                        res.send(data_tf);
                        sql.close();
                    }
                } //end else
            });
        }
    });
}

//GET ALL ACTIVE USERS FOR PATHWAYS
app.get("/users", function (req, res) {
    //var query = "select * from LMS.dbo.Users where OrgID = 100";
    var query = "SELECT TOP (200) ProductID, ProductName, ProductPrice FROM Products";
    executeQuery(query, res);

});

//GET ONE USER
app.get("/users/:ID_EMPLOYEE/", function (req, res) {
     var query = "SELECT ID_EMPLOYEE, USERNAME, PASSWORD, NAME FROM  dbo.[HR-USER] WHERE " + req.params.ID_EMPLOYEE + " GROUP BY USERNAME, PASSWORD, NAME, ID_EMPLOYEE";
     get_login(query, res);
});


//POST API
app.post("/api/user", function(req , res){
        console.log(req.body.Name);
        console.log(req.body.Email);
   // var query = "INSERT INTO [user] (Name,Email,Password) VALUES (req.body.Name,req.body.Email,req.body.Password”);
    //executeQuery (res, query);
});


